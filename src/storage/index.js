import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex)

import fishid from './fishid'

export default new Vuex.Store({
    state:{
        ...fishid['state']
    },
    mutations:{
        ...fishid['mutations']
    },
    actions: {
        ...fishid['actions']
    }
});
