import fishid from "@/fishid/sdk.js";

export default {
    state: {
        token: null,
        username: null,
        avatar: null,
        email: null,
    },
    actions: {
        login: function({commit,state},params){
            // onDone(token),onErr(isRemoteError,errObj)
            let {onDone,onErr} = params;
            params['onDone'] = null;
            params['onErr'] = null;
            return fishid.login(params)
                .then(function(json){
                    console.log(json)
                    if(json['ok'] != true){
                        onErr(true,json['err']);
                    }
                    commit('changeToken',json['token']['token'])
                    commit('changeUsername',json['token']['username'])
                    onDone(json['token']['token']);
                    return json;
                })
                .catch(function(error){
                    console.log(onErr)
                    onErr(false,error);
                });
        },
        updateUserInfo({commit,state}){
            return fishid.getUserInfo({
                token: state.token,
                keys: ['avatar','email']
            })
                .then((response) => {
                    if (response.data.ok){
                        commit('updateUserInfo',response.data.result);
                    } else {
                        console.log("Got the user info response but backend result not ok",response);
                    }
                })
                .catch((error) => {
                    if (error.response){
                        console.log("Got the reponse but status not ok",error);
                    } else {
                        console.log("Could not got response",error);
                    }
                });
        }
    },
    mutations: {
        changeToken(state,token){
            state.token = token;
        },
        changeUsername(state,username){
            state.username = username;
        },
        updateUserInfo(state,map){
            state.avatar = map['avatar'];
            state.email = map['email'];
        }
    }
}
