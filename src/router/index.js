import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Permit from '@/components/Permit'
import Ok from '@/components/Ok'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: "/login"
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/permit',
      name: 'Permit',
      component: Permit
    },
    {
      path: '/ok',
      name: 'Ok',
      component: Ok
    }
  ]
})
