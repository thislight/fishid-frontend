// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import Vuelidate from 'vuelidate'


Vue.config.productionTip = false

Vue.use(VueMaterial)
Vue.use(Vuelidate)
import storage from './storage'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: storage,
  components: { App },
  template: '<App/>'
})
