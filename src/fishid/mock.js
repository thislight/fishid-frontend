import MockAdapter from 'axios-mock-adapter'


let acceptMock = function (axios) {
    let adapter = new MockAdapter(axios);

    adapter.onPost('/api/login').reply((config) => {
        return [200,
            {
                ok: true,
                token: {
                    'username': 'thislight',
                    'token': 'dahsgadkgsdchjsghxdgaugduw'
                }
            }];
    });

    adapter.onPost('/api/appAuthInfo').reply(() => {
        return [200, {
            ok: true,
            app: {
                name: 'TestApp',
                symbol: 'testapp',
                callback: {
                    method: 'url',
                    url: 'https://example.com/auth/fishid/back'
                },
                permissions: ['Javascript:WriteSomething']
            },
            token: 'dgakjsbdkjgasgdkhjsd'
        }
        ]
    });

    adapter.onPost('/api/auth').reply(() => {
        return [200, {
            ok: true,
            callback: {
                method: 'url',
                url: 'https://example.com/auth/fishid/back'
            },
            token: {
                token: 'dgakjsbdkjgasgdkhjsd'
            }
        }];
    });

    adapter.onPost('/api/storage/fishid/get').reply(() => {
        return [200, {
            ok: true,
            result: {
                avatar: 'http://up.qqjia.com/z/face01/face06/facejunyong/junyong02.jpg',
                email: 'example@example.com'
            }
        }]
    })
}


export default acceptMock;
