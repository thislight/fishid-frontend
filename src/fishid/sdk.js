import axios from "axios";
import acceptMock from '@/fishid/mock';

const BASEURL = "/api/" 

const MOCK_REQUIRED = true

let _q = axios.create({
    baseURL: BASEURL,
})

if (MOCK_REQUIRED){
    acceptMock(_q)
}

let login = function (params) {
    let user = params['user'];
    let pass = params['pass'];
    let {resolveP, rejectP} = [null,null];
    let p = new Promise((resolve,reject) => {
        resolveP = resolve;
        rejectP = reject;
    });
    _q.post('/login',{
        'user': user,
        'password': pass,
    }).then((response) => {
        resolveP(response.data);
    }).catch(rejectP);
    return p;
}


let getAuthInfo = function(params){
    /*
    struct auth_info_api_params {
        token,
    }
    */
    let symbol = params['symbol'];
    return _q.post('/appAuthInfo',params);
}


let doAuth = function(params){
    /*
    struct auth_api_params {
        token,
        symbol,
        message,
    }
    */
   return _q.post('/auth',params);
}


let getUserInfo = function(params){
    return _q.post('/api/storage/fishid/get', params);
}


export default {
    login,
    getAuthInfo,
    doAuth,
    getUserInfo,
}
